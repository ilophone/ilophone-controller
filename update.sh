#!/usr/bin/env bash
#
#

# This script updates local libraries source files to the main source directory

cd $(dirname $0)

# Main source directory
MAIN_SRC="MIDI_CONTROLLER"

# uClock source directory
UCLOCK_SRC="uClock/src"

# ClickButton source directory
CLICKBUTTON_SRC="ClickButton"

# uClock original source files
ORIGINAL_UCLOCK_H="uClock.h"
ORIGINAL_UCLOCK_CPP="uClock.cpp"

# ClickButton original source files
ORIGINAL_CLICKBUTTON_H="ClickButton.h"
ORIGINAL_CLICKBUTTON_CPP="ClickButton.cpp"

# ClickButton destination source files
DEST_CLICKBUTTON_H="clickButton.h"
DEST_CLICKBUTTON_CPP="clickButton.cpp"

# Check if file already
# exists, then remove it
CheckAndRemove () {
  if [ -f "$MAIN_SRC/$1" ]; then
    rm "$MAIN_SRC/$1"
    echo "$MAIN_SRC/$1 as been removed"
  fi
}

# Check list
CheckAndRemove $ORIGINAL_UCLOCK_H
CheckAndRemove $ORIGINAL_UCLOCK_CPP
CheckAndRemove $DEST_CLICKBUTTON_H
CheckAndRemove $DEST_CLICKBUTTON_CPP

# Update uClock source files
cp $UCLOCK_SRC/$ORIGINAL_UCLOCK_H $MAIN_SRC
cp $UCLOCK_SRC/$ORIGINAL_UCLOCK_CPP $MAIN_SRC

echo "uClock source files are up-to-date"

# Update ClickButton source files
cp $CLICKBUTTON_SRC/$ORIGINAL_CLICKBUTTON_H $MAIN_SRC/$DEST_CLICKBUTTON_H
cp $CLICKBUTTON_SRC/$ORIGINAL_CLICKBUTTON_CPP $MAIN_SRC/$DEST_CLICKBUTTON_CPP

echo "ClickButton source files are up-to-date"

exit 0 # Success
