/*!
    @file       shift.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef SHIFT_H__
#define SHIFT_H__

#include "Arduino.h"
#include "CONFIG.h"

#include "clickButton.h"

extern ClickButton buttonObject;

#define SHIFT_PIN 12
#define TIME_TO_DOUBLE_CLICK 200

enum {
  s_RELEASED,
  s_PRESSED,
  s_DOUBLE_CLICKED
};

extern uint8_t buttonState;
extern bool shifToken;
extern uint32_t lastShift;

void shiftSetup(void);
void processShift(void);
void processShift_shift(void);
void shiftLoop(uint8_t *layer, uint8_t *octave);
bool shiftIsPressed(void);
void shiftUpdate(void);


#endif /* SHIFT */
