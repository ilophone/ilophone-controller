/*!
    @file       MIDI_CONTROLLER.ino
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "MIDI_CONTROLLER.h"


void setup()
{
  // Ensure the
  // MCU is ready
  bootSecurity(1500);

  // Init global variables
  curModule = DEFAULT_MODULE;
  layer = curModule / 10;
  channelStrip = curModule % 10;
  octave = DEFAULT_OCTAVE;

  // Pre-processing setup
  #ifdef CLOCK_ENABLED
    clockSetup();
  #endif
  ledSetup();
  encoderSetup();
  touchSetup();
  #ifdef ANALOG_ENABLED
    analogSetup();
  #endif
  shiftSetup();


  // If all above succeed,
  // light up the on-board LED
  digitalWrite(LED_BUILTIN, HIGH);
}

// =============================================================================

void loop()
{
  // Processes
  processLed();
  processEncoder();
  processTouch();
  #ifdef ANALOG_ENABLED
    processAnalog();
  #endif
  processShift();

  // Alternative processes,
  // if shift button is pressed
  while (shiftIsPressed() && !shifToken) {
    processLed_shift();
    processEncoder_shift();
    processTouch_shift();
    #ifdef ANALOG_ENABLED
      processAnalog();
    #endif
    processShift();
  }


  // MIDI Controllers should discard incoming MIDI messages.
  // http://forum.pjrc.com/threads/24179-Teensy-3-Ableton-Analog-CC-causes-midi-crash
  while (usbMIDI.read()); // Ignore incoming messages
}
