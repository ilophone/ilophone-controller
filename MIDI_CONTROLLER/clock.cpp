/*!
    @file       clock.cpp
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "clock.h"

float beatsPerMinute = DEFAULT_BPM;

bool tickPolarity;
bool runClock;

void clockSetup() {
  // Init variables
  runClock = false;
  tickPolarity = false;

  // Send a MIDI-RT stop
  // message on clock setup
  usbMIDI.sendRealTime(usbMIDI.Stop, 0);

  // Drift for USB Teensy
  uClock.setDrift(1);
  // Inits the clock
  uClock.init();
  // Set the callback function for the clock output to send MIDI Sync message.
  uClock.setClock96PPQNOutput(ClockOut96PPQN);
  // Set the callback function for MIDI Start and Stop messages.
  uClock.setOnClockStartOutput(onClockStart);  
  uClock.setOnClockStopOutput(onClockStop);
  // Set the clock BPM
  uClock.setTempo(beatsPerMinute);
  // If runClock, start the clock
  if (runClock) uClock.start();
}

// =============================================================================

// The callback function wich will be called
// by Clock each Pulse of 96PPQN clock resolution
void ClockOut96PPQN(uint32_t * tick) {
  // Send MIDI_CLOCK to external gears
  usbMIDI.sendRealTime(usbMIDI.Clock);
  // Current hack to sync
  // LEDs blink and clock
  if ((* tick) % 24 == 0)
    tickPolarity = true;
}

// =============================================================================

// The callback function wich will be called
// when clock starts by using Clock.start() method
void onClockStart() {
  usbMIDI.sendRealTime(usbMIDI.Start);
}

// =============================================================================

// The callback function wich will be called
// when clock stops by using Clock.stop() method
void onClockStop() {
  usbMIDI.sendRealTime(usbMIDI.Stop);
}

// =============================================================================

// Update uClock tempo
// by passing the internal BPM value
void bpmUpdate(uint16_t beatsPerMinute) {
  uClock.setTempo(beatsPerMinute);
}
