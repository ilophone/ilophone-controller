/*!
    @file       encoder.cpp
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021

    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "encoder.h"

Encoder *encoder[sizeof(TEENSY_ENC_PINS) / 2];

int newPositionEnc[sizeof(TEENSY_ENC_PINS) / 2];
int oldPositionEnc[sizeof(TEENSY_ENC_PINS) / 2];

long lastRotationTime[sizeof(TEENSY_ENC_PINS) / 2];


// Encoders configuration. This function creates
// new Encoder objects, inits old positions and reads new ones
void encoderSetup() {
  for (uint8_t i = 0; i < sizeof(TEENSY_ENC_PINS); i++) {
    // Create new Encoder objects
    encoder[i / 2] = new Encoder(TEENSY_ENC_PINS[i], TEENSY_ENC_PINS[i + 1]);
    // Init all old positions
    // to a far and non-zero value
    oldPositionEnc[i / 2] = -999;
    // update : i+=2 in the for loop ?
    i++;
  }

  for (uint8_t i = 0; i < sizeof(TEENSY_ENC_PINS) / 2; i++) {
    // Update value
    encoderUpdate(i);
    // Update old position
    // if new one is different
    if (encoderHasChanged(i))
      updateOlds(i);
  }
}

// =============================================================================

// Main feature
// process function
void processEncoder() {
  // Update encoder value update
  for (uint8_t i = 0; i < sizeof(TEENSY_ENC_PINS) / 2; i++) {
    encoderUpdate(i);
    // If value has changed,
    // send a corresponding MIDI-CC message on current channel
    if (encoderHasChanged(i))
      usbMIDI.sendControlChange(CC_OFFSET[layer][CC_ENCODER] + i, encoderToMidiMode(encoderGetRelativeValue(i)), channelStrip, 0);
  }
}

// =============================================================================

// Alternative feature
// process function (shift)
void processEncoder_shift() {
  // Update encoder value update
  for (uint8_t i = 0; i < sizeof(TEENSY_ENC_PINS) / 2; i++) {
    encoderUpdate(i);
    // If value has changed ...
    if (encoderHasChanged(i)) {
      if (curModule != MACRO_MODULE)
        usbMIDI.sendControlChange(CC_OFFSET[layer][CC_ENCODER] + i + 8, encoderToMidiMode(encoderGetRelativeValue(i)), channelStrip, 0);
      else // If we're on top-level layer
        usbMIDI.sendProgramChange(encoderToMidiMode(encoderGetRelativeValue(i)), i, 0);
    }
  }
}

// =============================================================================

// Update encoder value
// from the Encoder library
void encoderUpdate(uint8_t whichEncoder) {
  // Ensure whichEncoder index is not out of range
  if (whichEncoder > sizeof(TEENSY_ENC_PINS) / 2) return;
  // Read new positions value
  // divided by the encoder quadrature
  newPositionEnc[whichEncoder] = encoder[whichEncoder]->read() / QUADRATURE;
}

// =============================================================================

// Return if encoder has changed
bool encoderHasChanged(uint8_t whichEncoder) {
  // Ensure whichEncoder index is not out of range
  if (whichEncoder > sizeof(TEENSY_ENC_PINS) / 2) return false;
  // Compare current position with old one
  if (newPositionEnc[whichEncoder] != oldPositionEnc[whichEncoder]) return true;
  else return false;
}

// =============================================================================

// Return speed acceleration
// from the last rotation time
uint8_t getSpeedAcceleration(uint8_t whichEncoder) {
  // Ensure whichEncoder index is not out of range
  if (whichEncoder > sizeof(TEENSY_ENC_PINS) / 2) return 0;
  // How long ago the last rotation took place ?
  uint8_t howLong = millis() - lastRotationTime[whichEncoder];
  // Constrain howLong between zero
  // and the maximum time threshold (ms)
  uint8_t ratio = constrain(howLong, 0, int(MAX_TIME_ACC));
  // Compute the linear speed acceleration :
  // f(ratio) = MAX_SPEED_ACC - [( MAX_SPEED_ACC - 1 ) / MAX_TIME_ACC * ratio ]
  float speedAcceleration = MAX_SPEED_ACC - (float(MAX_SPEED_ACC) - 1) / MAX_TIME_ACC * ratio;
  // Force int on return
  return uint8_t(speedAcceleration);
}

// =============================================================================

// Update old positions
// and last rotation times with current ones
void updateOlds(uint8_t whichEncoder) {
  // Ensure whichEncoder index is not out of range
  if (whichEncoder > sizeof(TEENSY_ENC_PINS) / 2) return;
  // Update old positions
  oldPositionEnc[whichEncoder] = newPositionEnc[whichEncoder];
  // Update last rotation
  // time to the current one.
  lastRotationTime[whichEncoder] = millis();
}

// =============================================================================

// Get encoder relative value,
// update old positions and last rotation time
int8_t encoderGetRelativeValue(uint8_t whichEncoder) {
  // Ensure whichEncoder index is not out of range
  if (whichEncoder > sizeof(TEENSY_ENC_PINS) / 2) return 0;
  // Get sign from rotation direction
  int8_t sign = constrain(newPositionEnc[whichEncoder] - oldPositionEnc[whichEncoder], -1, 1);
  // Compute encoder value from minimal step, speed acceleration and sign
  int8_t encoderValue = ENCODER_MIN_STEP * getSpeedAcceleration(whichEncoder) * sign;
  // See function declaration above
  updateOlds(whichEncoder);
  // Return computed value
  return encoderValue;
}

// =============================================================================

// Return corresponding MIDI relative mode
// protocol value from computed encoder value
uint8_t encoderToMidiMode(int8_t encoderValue) {
  // If sign negative
  if (encoderValue < 0) {
    // Turn encoder value unsigned
    encoderValue = abs(encoderValue);
    // Check MIDI relative mode
    switch (MIDI_MODE) {
      // Absolute mode
      case ABSOLUTE:
        return encoderValue % 128;
      // Relative mode #1
      case RELATIVE_1:
        return constrain(128 - encoderValue, 65, 127);
      // Relative mode #2
      case RELATIVE_2:
        return constrain(64 - encoderValue, 0, 63);
      // Relative mode #3
      case RELATIVE_3:
        return constrain(64 + encoderValue, 65, 127);
      // Not defined
      default:
#ifdef DEBUG_ENABLED
        Serial.println(F("ERROR: WRONG MIDI_RELATIVE_MODE"));
#endif
        while (1);
        break;
    }
  }

  else {  // If sign positive
    // Check MIDI relative mode
    switch (MIDI_MODE) {
      // Absolute mode
      case ABSOLUTE:
        return encoderValue % 128;
      // Relative mode #1
      case RELATIVE_1:
        return constrain(encoderValue, 1, 64);
      // Relative mode #2
      case RELATIVE_2:
        return constrain(64 + encoderValue, 65, 127);
      // Relative mode #3
      case RELATIVE_3:
        return constrain(encoderValue, 1, 64);
      // Not defined
      default:
#ifdef DEBUG_ENABLED
        Serial.println(F("ERROR: WRONG MIDI_RELATIVE_MODE"));
#endif
        while (1);
        break;
    }
  }
}
