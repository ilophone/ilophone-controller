/*!
    @file       MIDI_CONTROLLER.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021

    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef MIDI_CONTROLER_H__
#define MIDI_CONTROLER_H__

#include "CONFIG.h"

#include "encoder.h"
#include "leds.h"
#include "shift.h"
#include "touch.h"
#include "analog.h"
#include "clock.h"

// Global vars
uint8_t curModule;
uint8_t layer;
uint8_t channelStrip;
uint8_t octave;

// Ensure the MCU is ready,
// Wait for waitTime (ms) since pgm started
void bootSecurity(uint16_t waitTime) {
  // Setup the internal LED
  pinMode(LED_BUILTIN, OUTPUT);
  // Internal LED blinks
  while (millis() < waitTime * 2 / 3)
    digitalWrite(LED_BUILTIN, int((millis() / 25) % 2));
  // Turn off the internal LED
  digitalWrite(LED_BUILTIN, LOW);
  // Wait
  delay(waitTime / 3);
}


#endif /* MIDI_CONTROLER */
