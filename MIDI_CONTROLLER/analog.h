/*!
    @file       analog.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef ANALOG_H__
#define ANALOG_H__

#include "Arduino.h"
#include "CONFIG.h"

#include "shift.h"

#include <ResponsiveAnalogRead.h>

// Compute the maximum analog value from ANALOG_RES
const byte MAX_ANALOG_VALUE = pow(2, ANALOG_RES) - 1;

extern ResponsiveAnalogRead *analog[sizeof(ANALOG_PINS)];

void analogSetup(void);
void processAnalog(void);
void processAnalog_shift(void);
void analogUpdate(uint8_t whichAnalog);
uint16_t analogGetValue(uint8_t whichAnalog);
bool analogHasChanged(uint8_t whichAnalog);


#endif /* ANALOG */
