/*!
    @file       clock.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CLOCK_H__
#define CLOCK_H__

#include "Arduino.h"
#include "CONFIG.h"

#include "uClock.h"

extern float beatsPerMinute;

extern bool tickPolarity;
extern bool runClock;

void clockSetup();
void bpmUpdate(uint16_t beatsPerMinute);

void ClockOut96PPQN(uint32_t * tick);
void onClockStart(void);
void onClockStop(void);


#endif /* CLOCK */
