/*!
    @file       touch.cpp
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021

    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "touch.h"

Adafruit_MPR121 *MPR121[sizeof(MPR121_ADDR)];

unsigned int currtouched[sizeof(MPR121_ADDR)];
unsigned int lasttouched[sizeof(MPR121_ADDR)];

uint16_t maxMprAdcValue;
uint16_t minMprAdcValue;
uint16_t rangeMprAdcValue;
uint16_t calibCounter;

long lastDebounce;
bool debounceToken;

boolean toggleState[sizeof(BLACK_KEYS)];

const uint8_t asciiOffset = 48;

uint8_t buf_load[] = {0xF0, '/', 'm', 'o', 'd', 'u', 'l', 'e', '/', '0', '/', 'l', 'o', 'a', 'd', 0xF7};
uint8_t buf_save[] = {0xF0, '/', 'm', 'o', 'd', 'u', 'l', 'e', '/', '0', '/', 's', 'a', 'v', 'e', 0xF7};
uint8_t buf_ok[] = {0xF0, '/', 'm', 'o', 'd', 'u', 'l', 'e', '/', '0', '/', 'o', 'k', 0xF7};


// Adafruit MPR_121 module configuration. This function
// inits new Adafruit_MPR121 objects and return a Serial
// error message if one or both modules are not availables.
void touchSetup() {
  // Call MPR_121 module(s) to begin
  for (uint8_t i = 0; i < sizeof(MPR121_ADDR); i++)
    if (!(MPR121[i] = new Adafruit_MPR121())->begin(MPR121_ADDR[i])) {
      // Print debug informations
#ifdef DEBUG_ENABLED
      Serial.print(F("ERROR: ADAFRUIT MPR_121 n°"));
      Serial.print(i + 1);
      Serial.print(F("/"));
      Serial.print(sizeof(MPR121_ADDR));
      Serial.print(F(" "));
      Serial.print(F("Failed to begin. Please check wiring."));
#endif
      // Stop pgm until MPR_121 could begin (slow blinking internal LED)
      while (!(MPR121[i] = new Adafruit_MPR121())->begin(MPR121_ADDR[i]))
        digitalWrite(LED_BUILTIN, int((millis() / 250) % 2));
    }

  // Calibration tools
  maxMprAdcValue = 0;
  minMprAdcValue = MAX_MPR121_ADC_VALUE;
  // De-bounce tools
  lastDebounce = millis();
  debounceToken = false;
  // Init toggle state
  for (uint8_t i = 0; i < sizeof(BLACK_KEYS); i++)
    toggleState[i] = false;
}

// =============================================================================

// Main feature
// process function
void processTouch() {
  // Get current touch states for each modules
  touchUpdate();
  // Remove any bounce
  touchDebounce();
  // If shift is pressed,
  // force alternative process
  if (shiftIsPressed()) {
    updateLastTouched();
    return;
  }

  // If octave down is touched
  if (isTouched(OCT_DOWN) == t_TOUCHED && debounceToken) {
    octave--; // Decrement global octave variable
    octave = constrain(octave, 1, 5); // constrain octave level [1-5]
    // Global note-Off
    for (uint8_t i = 0; i < 128; i++)
      usbMIDI.sendNoteOff(i, 0, channelStrip, 0);
    // Update debounce last time
    updateForDebounce();
  }

  // If octave up is touched
  if (isTouched(OCT_UP) == t_TOUCHED && debounceToken) {
    octave++; // Increment global octave variable
    octave = constrain(octave, 1, 5); // constrain octave level [1-5]
    // Global note-Off
    for (uint8_t i = 0; i < 128; i++)
      usbMIDI.sendNoteOff(i, 0, channelStrip, 0);
    // Update debounce last time
    updateForDebounce();
  }

  // If a musical key is touched (or multiple keys)
  for (uint8_t i = 0; i < sizeof(MUSICAL_KEYS); i++) {
    if (layer == 0) {
      if (isTouched(MUSICAL_KEYS[i]) == t_TOUCHED) {
        // If experimental velocity disabled,
        if (!EXPERIMENTAL_VELOCITY) // send a static velocity
          usbMIDI.sendNoteOn(LOWEST_MIDI_NOTE + octave * 12 + i, NOTE_VELOCITY, channelStrip, 0);
        else // If not, send a computed velocity
          usbMIDI.sendNoteOn(LOWEST_MIDI_NOTE + octave * 12 + i, getVelocity(MUSICAL_KEYS[i]), channelStrip, 0);
      }

      // If a musical key has been released,
      else if (isTouched(MUSICAL_KEYS[i]) == t_RELEASED) // send a note-off MIDI message
        usbMIDI.sendNoteOff(LOWEST_MIDI_NOTE + octave * 12 + i, 0, channelStrip, 0);
    }

    else if (layer > 0) {
      if (isTouched(MUSICAL_KEYS[i]) == t_TOUCHED)
        usbMIDI.sendControlChange(CC_OFFSET[layer][CC_PUSH] + i, 127, channelStrip, 0);
      else if (isTouched(MUSICAL_KEYS[i]) == t_RELEASED)
        usbMIDI.sendControlChange(CC_OFFSET[layer][CC_PUSH] + i, 0, channelStrip, 0);
    }
  }

  // Last touch
  // gets current
  updateLastTouched();
  // Update shift token
  if (!shiftIsPressed())
    shifToken = false;
}

// =============================================================================

// Alternative feature
// process function (shift)
void processTouch_shift() {
  // Get current touch states for each modules
  touchUpdate();

  if (isTouched(SIG_PLUS) == t_TOUCHED) {
    usbMIDI.sendControlChange(8, 1, MACRO_MODULE, 0);
  }

  if (isTouched(SIG_MIN) == t_TOUCHED) {
    usbMIDI.sendControlChange(8, -1, MACRO_MODULE, 0);
  }

  // If panic key is touched
  if (isTouched(PANIC_KEY) == t_TOUCHED) {
    if (curModule != MACRO_MODULE) {
      // Calibrate capacitive modules
      touchSetup();
      // Global note-Off
      for (uint8_t i = 0; i < 128; i++)
        usbMIDI.sendNoteOff(i, 0, channelStrip, 0);
    }
    // Force exiting the alt. process
    shifToken = true;
  }

  // If octave down is pressed
  if (isTouched(OCT_DOWN) == t_TOUCHED) {
    // If we're not on top-level layer
    if (curModule != MACRO_MODULE) {
      // Send a MIDI-SysEx message with a load String message
      buf_load[9] = uint8_t(channelStrip + asciiOffset);
      usbMIDI.sendSysEx(sizeof(buf_load), buf_load, true, 0);
      // Force exiting the alt. process
      shifToken = true;
    }

    else { // If we're on top-level layer
      // If BPM superior to 120
      if (beatsPerMinute > 120)
        // Decrement BPM 6 by 6
        bpmUpdate(beatsPerMinute -= 6);
      else // If BPM inferior to 120
        // Decrement BPM 4 by 4
        bpmUpdate(beatsPerMinute -= 4);
    }
  }

  // If octave up is pressed
  if (isTouched(OCT_UP) == t_TOUCHED) {
    // If we're not on top-level layer
    if (curModule != MACRO_MODULE) {
      // Send a MIDI-SysEx message with a save String message
      buf_save[9] = uint8_t(channelStrip + asciiOffset);
      usbMIDI.sendSysEx(sizeof(buf_save), buf_save, true, 0);
      // Force exiting the alt. process
      shifToken = true;
    }

    else { // If we're on top-level layer
      // If BPM superior to 120
      if (beatsPerMinute > 120)
        // Increment BPM 6 by 6
        bpmUpdate(beatsPerMinute += 6);
      else // If BPM inferior to 120
        // Increment BPM 4 by 4
        bpmUpdate(beatsPerMinute += 4);
    }
  }

  // Check for moduleTouchPins, update the module
  for (uint8_t i = 0; i < HOW_MANY_CHANNELS; i++) {
    if (isTouched(WHITE_KEYS[i]) == t_TOUCHED) {
      // If we're on the top-level layer
      if (curModule == 0) curModule = i + 1;
      // If we're on main level layer
      else if (channelStrip == i + 1 && layer < HOW_MANY_LAYERS - 1) curModule += 10;
      // If we're on any last alt. level layer
      else if (layer >= HOW_MANY_LAYERS - 1) curModule = i + 1;
      // Other
      else curModule = i + 1;
      // Update the layer
      layer = curModule / 10;
      // Update the channel
      channelStrip = curModule % 10;
      // Send note-off value with channelStrip
      // to notify that curModule has been changed
      usbMIDI.sendNoteOff(0, 0, channelStrip, 0);
      // Force exiting the alt. process0,
      shifToken = true;
    }
  }

  if (curModule == MACRO_MODULE)
  { // If we're on top-level layer
    // If the first black key is touched
    if (isTouched(BLACK_KEYS[0]) == t_TOUCHED) {
      // Change clock state
      runClock = !runClock;
      // Stop running clock if needed
      if (!runClock) {
        uClock.stop();
        ledsOff();
      }
      // Start clock otherwise
      else uClock.start();
    }

    // If the 2nd black key is touched
    if (isTouched(BLACK_KEYS[2]) == t_TOUCHED) {
      // Send a MIDI-SysEx message with a save String message
      buf_save[9] = uint8_t(channelStrip + asciiOffset);
      usbMIDI.sendSysEx(sizeof(buf_save), buf_save, true, 0);
      // Force exiting the alt. process
      shifToken = true;
    }

    // If the 3rd black key is touched
    if (isTouched(BLACK_KEYS[3]) == t_TOUCHED) {
      // Send a MIDI-SysEx message with a load String message
      buf_load[9] = uint8_t(channelStrip + asciiOffset);
      usbMIDI.sendSysEx(sizeof(buf_load), buf_load, true, 0);
      // Force exiting the alt. process
      shifToken = true;
    }

    // If the 4th black key is touched
    if (isTouched(BLACK_KEYS[4]) == t_TOUCHED) {
      // Send a MIDI-SysEx message with a enter String message
      buf_ok[9] = uint8_t(channelStrip + asciiOffset);
      usbMIDI.sendSysEx(sizeof(buf_ok), buf_ok, true, 0);
      // Force exiting the alt. process
      shifToken = true;
    }
  }

  else { // If we're not on top-level layer
    // If any black keys is touched
    for (uint8_t i = 0; i < sizeof(BLACK_KEYS); i++) {
      if (isTouched(BLACK_KEYS[i]) == t_TOUCHED) {
        // Update toggle states
        toggleState[i] = !toggleState[i];
        // Send the corresponding MIDI-CC message
        usbMIDI.sendControlChange(CC_OFFSET[layer][CC_TOGGLE] + i, toggleState[i] * TOGGLE_VELOCITY, channelStrip, 0);
        // Force exiting the alt. process
        shifToken = true;
      }
    }
  }

  // Last touch
  // gets current
  updateLastTouched();
}

// =============================================================================

// Get current touch states for each modules
void touchUpdate() {
  for (uint8_t i = 0; i < sizeof(MPR121_ADDR); i++)
    currtouched[i] = MPR121[i]->touched();
}

// =============================================================================

// Check for debounce
void touchDebounce() {
  if (millis() - lastDebounce > DEBOUNCE && !debounceToken)
    debounceToken = true;
}

// =============================================================================

// Update debounce time
void updateForDebounce() {
  debounceToken = false;
  lastDebounce = millis();
}

// =============================================================================

// Update last touch state for each modules
void updateLastTouched() {
  for (uint8_t i = 0; i < sizeof(MPR121_ADDR); i++)
    lasttouched[i] = currtouched[i];
}

// =============================================================================

// Check if specific Adafruit MPR_121 key is touched and
// return touch state [ t_TOUCHED = 2 | t_RELEASED = 1 ]
uint8_t isTouched(uint8_t key) {
  // Check if key is not out of range
  if (key > sizeof(MPR121_ADDR) * HOW_MANY_KEY_PER_MODULE - 1) return 0;
  // Which MPR ?
  uint8_t MPR = key / HOW_MANY_KEY_PER_MODULE;
  // Which key ?
  key = key % HOW_MANY_KEY_PER_MODULE;
  // If touched but wasn't last time
  if ((currtouched[MPR] & (1 << key)) && !(lasttouched[MPR] & (1 << key))) return t_TOUCHED;
  // If released but wasn't last time
  else if (!(currtouched[MPR] & (1 << key)) && (lasttouched[MPR] & (1 << key))) return t_RELEASED;
  // If undefined
  else return 0;
}

// =============================================================================

// Check if specific Adafruit MPR_121
// key is held and returns hold state
bool isHeld(uint8_t key) {
  // Check if key is not out of range
  if (key > sizeof(MPR121_ADDR) * HOW_MANY_KEY_PER_MODULE - 1) return false;
  // Which MPR ?
  uint8_t MPR = key / HOW_MANY_KEY_PER_MODULE;
  // Which key ?
  key = key % HOW_MANY_KEY_PER_MODULE;
  // If held
  if ((currtouched[MPR] & (1 << key))) return true;
  // If released
  else if (!(currtouched[MPR] & (1 << key))) return false;
  // If undefined
  else return false;
}

// =============================================================================

// Deduce a velocity from MPR's raw ADC value and return
// a pseudo-velocity from MPR's class function : filteredData or baselineData
uint8_t getVelocity(uint8_t key) {
  // Check if key is not out of range
  if (key > sizeof(MPR121_ADDR) * HOW_MANY_KEY_PER_MODULE - 1) return 0;

  // Which MPR ?
  uint8_t MPR = key / HOW_MANY_KEY_PER_MODULE;
  // Which key ?
  key = key % HOW_MANY_KEY_PER_MODULE;

  // Velocity based on filteredData
  uint16_t mprAdcRawValue = MPR121[MPR]->filteredData(key);
  // Velocity based on baselineData
  //uint16_t mprAdcRawValue = MPR121[MPR]->baselineData(key);

  // Calibration,
  // Update maximum ADC value
  if (mprAdcRawValue > maxMprAdcValue) {
    maxMprAdcValue = mprAdcRawValue;
    rangeMprAdcValue = maxMprAdcValue - minMprAdcValue;
  }

  // Calibration,
  // Update minimum ADC value
  if (mprAdcRawValue < minMprAdcValue) {
    minMprAdcValue = mprAdcRawValue;
    rangeMprAdcValue = maxMprAdcValue - minMprAdcValue;
  }

  // Scale MPR's RAW datas to get a between 0 and 127 value
  float velocity = abs(float(mprAdcRawValue - minMprAdcValue) / rangeMprAdcValue * NOTE_VELOCITY - NOTE_VELOCITY);
  // Get rid of infinite values
  velocity = uint8_t(constrain(velocity, 0, NOTE_VELOCITY));

  // Check if the range learning is complete
  if (calibCounter < EXPERIMENTAL_VELOCITY) {
    // Until it's over
    velocity = NOTE_VELOCITY;
    // Increment the calibration checker
    calibCounter++;
  }

  // Check learning with Serial
#ifdef DEBUG_ENABLED
  Serial.print(mprAdcRawValue);
  Serial.print("\t");
  Serial.print(minMprAdcValue);
  Serial.print("\t");
  Serial.print(maxMprAdcValue);
  Serial.print("\t");
  Serial.println(velocity);
#endif


  return velocity;
}
