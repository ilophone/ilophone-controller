/*!
    @file       leds.cpp
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "leds.h"

unsigned int ledState[] = { 0, 0 };


// LEDs configuration. This function inits
// the Teensy's dedicated pins in OUTPUT mode.
void ledSetup() {
  for (uint8_t i = 0; i < sizeof(LED); i++)
    pinMode(LED[i], OUTPUT);
}

// =============================================================================

// Main feature
// process function
void processLed() {
  // Show the octave level
  octave2LedState(octave);
}

// =============================================================================

// Alternative feature
// process function (shift)
void processLed_shift() {
  // If clock is running
  // ping pong based on clock tick polarity
  if (runClock) ledPingPong(tickPolarity);
  // If not, turn LEDs off
  else ledsOff();
  // Update tickPolarity token
  tickPolarity = false;
}

// =============================================================================

// Write state of LEDs
void ledUpdate(uint8_t whichLed) {
  // Ensure whichLed is not out of range
  if (whichLed > sizeof(LED)) return;
  // Write ledState on its corresponding analog port
  analogWrite(LED[whichLed], ledState[whichLed]);
}

// =============================================================================

// Turn octave levels into
// brightness state for both LEDs
void octave2LedState(uint8_t octave) {
  for (uint8_t i = 0; i < sizeof(LED); i++) {
    ledState[i] = LED_SHINE[constrain((1 - 2 * i) * (octave) - 1 + 6 * i, 0, 2)];
    ledUpdate(i);
  }
}

// =============================================================================

// Light off both LEDs
void ledsOff() {
  for (uint8_t i = 0; i < sizeof(LED); i++) {
    ledState[i] = 0;
    ledUpdate(i);
  }
}

// =============================================================================

// LED ping pong effect
void ledPingPong(bool change) {
  // check if change token
  if (!change) return;
  // State #1
  if (ledState[1] == 0) {
    ledState[0] = 0;
    ledState[1] = 1023;
  }
  // State #2
  else if (ledState[1] == 1023) {
    ledState[0] = 1023;
    ledState[1] = 0;
  }
  // if not, turn off leds
  else {
    ledsOff();
    return;
  }
  // Write changes
  for (uint8_t i = 0; i < sizeof(LED); i++)
    ledUpdate(i);
}
