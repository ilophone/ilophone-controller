/*!
    @file       CONFIG.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CONFIG_H__
#define CONFIG_H__

//#define DEBUG_ENABLED


// Wiring diagram
// =============================================================================


/***


    
                                   ╔═══╗
                            ╔══════║   ║══════╗
              GND bus <---> ║G     ╚═══╝    5V║
               ENC_B7 <---> ║0            AGND║ <---> Analog GND bus
               ENC_A7 <---> ║1            3.3v║ <---> VCC bus
               ENC_B1 <---> ║2              23║ <---> ENC_A6
               ENC_A5 <---> ║3       T      22║ <---> ENC_B6
               ENC_B4 <---> ║4       E      21║ <---> ENC_A8
               ENC_A4 <---> ║5       E      20║ <---> ENC_B8
               ENC_B3 <---> ║6       N      19║ <---> [SCL] MPR_121
               ENC_A3 <---> ║7       S      18║ <---> [SDA] MPR_121
               ENC_B2 <---> ║8       Y      17║ <---> LED #2
               ENC_A2 <---> ║9              16║ <---> LED #1
               ENC_A1 <---> ║10             15║ <---> ADC #1
               ENC_B1 <---> ║11             14║ <---> ADC #2
                SHIFT <---> ║12             13║ <---> (Internal LED)
                            ╚═════════════════╝



***/


// Global variables
// =============================================================================

// Init variables
extern uint8_t curModule;
extern uint8_t layer;
extern uint8_t channelStrip;
extern uint8_t octave;

// Default values
const uint8_t MACRO_MODULE = 0;
const uint8_t DEFAULT_MODULE = MACRO_MODULE;
const uint8_t HOW_MANY_CHANNELS = 8;
const uint8_t HOW_MANY_LAYERS = 2;
const uint8_t DEFAULT_OCTAVE = 3;
const uint8_t ETP = 3;
const uint8_t CC_OFFSET[HOW_MANY_LAYERS][ETP] = { { 0, 16 }, { 24, 40, 47 } };

enum {
  CC_ENCODER,
  CC_TOGGLE,
  CC_PUSH
};


// Analog
// =============================================================================

//#define ANALOG_ENABLED // Un-comment to enable the analog feature

const uint8_t ANALOG_RES = 7;           // ADC Resolution in bits
const byte ANALOG_PINS[] = { A0, A1 };  // Teensy analog pinout
                                        //   + A12 if needed

// Clock
// =============================================================================

#define CLOCK_ENABLED // Un-comment to enable the clock feature

const uint8_t DEFAULT_BPM = 120; // Default BPM value

// Encoder
// =============================================================================

const uint8_t MAX_SPEED_ACC = 10;     // Maximum acceleration factor
const uint8_t MAX_TIME_ACC = 75;      // Maximum time threshold (ms) for acc.
const uint8_t ENCODER_MIN_STEP = 1;   // Encoder resolution

// LEDs
// =============================================================================

const int LED_SHINE[] = { 0, 40, 1023 }; // LED brightness thresholds

// Touch
// =============================================================================

// Default MIDI velocities
const uint8_t NOTE_VELOCITY = 127;
const uint8_t TOGGLE_VELOCITY = 127;

const uint8_t EXPERIMENTAL_VELOCITY = false;  // EXPERIMENTAL_VELOCITY :
                                              // =======================
                                              // 0 : Not in used (or false)
                                              // n : Number of iterations for
                                              // the calibration (default = 100)

// MIDI
// =============================================================================

/* 
 *  MIDI Relative modes :
 *  =====================
 *  #0 : Absolute % 128 (not relative)
 *  #1 : 1  = +1  ;  127 = -1
 *  #2 : 65 = +1  ;  63  = -1
 *  #3 : 1  = +1  ;  65  = -1
*/

enum MIDI_MODE {
  ABSOLUTE,
  RELATIVE_1,
  RELATIVE_2,
  RELATIVE_3
};

#define MIDI_MODE RELATIVE_3 // MIDI mode for encoders

const uint8_t LOWEST_MIDI_NOTE = 24;    // C1-32.703Hz
const uint8_t MAX_MIDI_CC_VALUE = 127;

// Buffers for MIDI SysEx sends
extern uint8_t buf_load[];
extern uint8_t buf_save[];
extern uint8_t buf_enter[];


#endif /* CONFIG */
