/*!
    @file       analog.cpp
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "analog.h"

ResponsiveAnalogRead *analog[sizeof(ANALOG_PINS)];


// Analog to MIDI feature configuration. This function sets
// the ADC resolution and inits new ResponsiveAnalogRead objects
void analogSetup() {
  // Set ADC analog resolution
  analogReadRes(ANALOG_RES);
  // Create new ResponsiveAnalogRead objects,
  // with sleep/edgeSnap enabled and correct analogRes
  for (uint8_t i = 0; i < sizeof(ANALOG_PINS); i++) {
    analog[i] = new ResponsiveAnalogRead(ANALOG_PINS[i], true);
    analog[i]->enableEdgeSnap();
    analog[i]->setAnalogResolution(pow(2, ANALOG_RES));
  }
}

// =============================================================================

// Main feature
// process function
void processAnalog() {
  // Update analog values
  for (uint8_t i = 0; i < sizeof(ANALOG_PINS); i++) {
    analogUpdate(i);
    // If value has changed,
    // send a corresponding MIDI-CC message on default layer
    if (analogHasChanged(i))
      usbMIDI.sendControlChange(i + 16, analogGetValue(i), MACRO_MODULE, 0); // BUG
  }
}

// =============================================================================

// Alternative feature
// process function (shift)
void processAnalog_shift() {
  // Nothing specific here
}

// =============================================================================

// Get and return analog value
// from the ResponsiveAnalogRead library
uint16_t analogGetValue(uint8_t whichAnalog) {
  // Ensure whichAnalog index is not out of range
  if (whichAnalog > sizeof(ANALOG_PINS)) return 0;
  // Scale the analogValue from the analog to the MIDI resolution
  float analogValue = float(analog[whichAnalog]->getValue()) / MAX_ANALOG_VALUE * MAX_MIDI_CC_VALUE;
  // Force int on return
  return int(analogValue);
}

// =============================================================================

// Update analog value
// from the ResponsiveAnalogRead library
void analogUpdate(uint8_t whichAnalog) {
  // Ensure whichAnalog index is not out of range
  if (whichAnalog > sizeof(ANALOG_PINS)) return;
  // Update whichAnalog value
  analog[whichAnalog]->update();
}

// =============================================================================

// Return if analog value has changed,
// from the ResponsiveAnalogRead library
bool analogHasChanged(uint8_t whichAnalog) {
  // Ensure whichAnalog index is not out of range
  if (whichAnalog > sizeof(ANALOG_PINS)) return 0;
  // Return if whichAnalog value has changed
  return analog[whichAnalog]->hasChanged();
}
