/*!
    @file       shift.cpp
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "shift.h"

ClickButton buttonObject(SHIFT_PIN, LOW, CLICKBTN_PULLUP);

uint8_t buttonState;
uint32_t lastShift;
bool shifToken;


// Shift button configuration. This function inits
// the Teensy's dedicated pins in INPUT_PULLUP mode.
void shiftSetup() {
  // Set pin mode
  pinMode(SHIFT_PIN, INPUT_PULLUP);
  // Set ClickButton object parameters
  buttonObject.debounceTime   = 20;
  buttonObject.multiclickTime = TIME_TO_DOUBLE_CLICK;
  buttonObject.longClickTime  = TIME_TO_DOUBLE_CLICK;
  // Init variables
  buttonState = 0;
  lastShift = millis();
  shifToken = false;
}

// =============================================================================

// Main feature
// process function
void processShift() {
  // Check button state
  shiftUpdate();
  // If double-clicked
  if (buttonState == 2) {
    // General layer
    curModule = 0;
    layer = curModule / 10;
    channelStrip = curModule % 10;
    // Send note-off value to notify that layer has been changed
    usbMIDI.sendNoteOff(0, 0, channelStrip, 0);
  }
}

// =============================================================================

void processShift_shift() {
  // Nothing specific here
}

// =============================================================================

// RAW simple function
bool shiftIsPressed() {
  // Return true if pressed (INPUT_PULLUP)
  if (!digitalRead(SHIFT_PIN)) return true;
  // False otherwise
  return false;
}

// =============================================================================

// Update shift
void shiftUpdate() {
  // Update ClickButton
  buttonObject.Update();
  // How many clicks ?
  buttonState = buttonObject.clicks;
}
