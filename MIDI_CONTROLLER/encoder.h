/*!
    @file       encoder.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef ENCODER_H__
#define ENCODER_H__

#include "Arduino.h"
#include "CONFIG.h"

#include "shift.h"

#include <Encoder.h>

#define QUADRATURE 4 // How many steps in the encoder quadrature

// Teensy-Encoders pinout config  ==>  { A1, B1, A2, B2, ... , An, Bn }
const uint8_t TEENSY_ENC_PINS[] = { 10, 11, 9, 8, 7, 6, 5, 4, 3, 2, 23, 22, 1, 0, 21, 20 };

extern Encoder *encoder[sizeof(TEENSY_ENC_PINS) / 2];

extern int newPositionEnc[sizeof(TEENSY_ENC_PINS) / 2];
extern int oldPositionEnc[sizeof(TEENSY_ENC_PINS) / 2];

extern long lastRotationTime[sizeof(TEENSY_ENC_PINS) / 2];

void encoderSetup(void);
void processEncoder(void);
void processEncoder_shift(void);
int8_t encoderGetRelativeValue(uint8_t whichEncoder);
void encoderUpdate(uint8_t whichEncoder);
bool encoderHasChanged(uint8_t whichEncoder);
uint8_t getSpeedAcceleration(uint8_t whichEncoder);
void updateOlds(uint8_t whichEncoder);
uint8_t encoderToMidiMode(int8_t encoderValue);


#endif /* ENCODER */
