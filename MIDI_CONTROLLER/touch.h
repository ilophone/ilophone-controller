/*!
    @file       touch.h
    @project    ILOPHONE Controller
    @brief      Part of the ILOPHONE environment, this is a MIDI-based
                controller designed to be a physical interface of Pure Data
    @version    1.0.0 (beta)
    @author     Martin Saëz
    @contact    martin.saez@woollystud.io
    @date       06/07/2021
    
    Copyright (C) 2021 Martin Saëz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef TOUCH_H__
#define TOUCH_H__

#include "Arduino.h"
#include "CONFIG.h"

#include "clock.h"
#include "leds.h"
#include "shift.h"

#include <Adafruit_MPR121.h>

#define MPR121_ADC_RES 10
#define HOW_MANY_KEY_PER_MODULE 12

#define OCT_DOWN 0
#define OCT_UP 1

#define PANIC_KEY 18
#define SPECIAL_KEY 16

#define SIG_PLUS 17
#define SIG_MIN 15

#define DEBOUNCE 100 // De-bounce time (ms)

enum {
  t_TOUCHED = 2,
  t_RELEASED = 1
};

const uint8_t MPR121_ADDR[] = { 0x5A, 0x5B };
const uint16_t MAX_MPR121_ADC_VALUE = pow(2, MPR121_ADC_RES) - 1;

extern Adafruit_MPR121 *MPR121[sizeof(MPR121_ADDR)];

const uint8_t MUSICAL_KEYS[] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };
const uint8_t WHITE_KEYS[] = { 2, 4, 6, 7, 9, 11, 13, 14, 16, 18 };
const uint8_t BLACK_KEYS[] = { 3, 5, 8, 10, 12, 15, 17 };

extern unsigned int currtouched[];
extern unsigned int lasttouched[];

extern uint16_t maxMprAdcValue;
extern uint16_t minMprAdcValue;
extern uint16_t rangeMprAdcValue;
extern uint16_t calibCounter;

extern long lastDebounce;
extern bool debounceToken;

extern boolean toggleState[];

void touchSetup(void);
void processTouch(void);
void processTouch_shift(void);
void touchUpdate(void);
void touchDebounce(void);
void updateForDebounce(void);
void updateLastTouched(void);

uint8_t isTouched(uint8_t key);
bool isHeld(uint8_t key);
uint8_t getVelocity(uint8_t key);


#endif /* TOUCH */
