# Ilophone - MIDI Controller

Designed to be a physical interface of :link: [Pure-data](https://puredata.info), this MIDI controller is part of the Ilophone environment. As it is a standard MIDI controller, you obviously can build it on your own and use it for another use.

It is made up of 8 rotary encoders, a musical keyboard with 17 musical keys, an octave manager, 2 LEDs and a shift button which brings some special features ...

![](Pictures/Ilophone-box.gif)

## Features

- A **65-keys MIDI keyboard** based on capacitive interaction
- 8x **Encoders** which can control until 144 different MIDI-CC capable parameters
- 7x MIDI-CC capable **toggles**
- 2x **Analog inputs** to MIDI-CC
- 2x **LEDs** to monitor the current octave level and some other operating states
- A **MIDI panic** function (which also re-calibrates the capacitive module)
- A 24fps **MIDI-clock** with start/stop and BPM increment functions
- A **pseudo note-velocity** based on the read capacity [ experimental ]

### Keyboard :musical_keyboard:

Composed of **17 musical keys** and an octave manager, you can play with **5 different octave levels**. Combined with the shift button, you can also :
- Change the current layer you are working on (shift + first eight white keys)
- Change the MIDI toggle state (shift + black keys)
- Run the MIDI panic function and calibrate the capacitive module (shift + last white musical key)

An experimental and additionnal feature also allows you to use the capacitive module RAW ADC values as the note velocity. To use it, edit the [CONFIG.h](MIDI_CONTROLLER/CONFIG.h) file and define **EXPERIMENTAL_VELOCITY** as a non-zero value :

``` c++
// Default MIDI velocities
const uint8_t NOTE_VELOCITY = 127;
const uint8_t TOGGLE_VELOCITY = 127;

const uint8_t EXPERIMENTAL_VELOCITY = 100;  // EXPERIMENTAL_VELOCITY :
                                            // =======================
                                            // 0 : Not in used (or false)
                                            // n : Number of iterations for
                                            // the calibration (default = 100)
```

  *This non-zero value consponds to the number of iteration necessary for the velocity calibration.*

### Layers

You can choose the MIDI channel you are working on from 1 to 8. We call it the "layer". You also can work on channel 17 (default) which is considered as the general layer. Is also exists 9 to 16 layers, they are linked to the first 8 in the Ilophone processes but they are MIDI independant channels.

You can edit the [CONFIG.h](MIDI_CONTROLLER/CONFIG.h) file to change the default initial layer :

``` c++
// Default values
const uint8_t DEFAULT_LAYER = 16;   //   <-- Edit the default layer here
const uint8_t DEFAULT_OCTAVE = 3;
```

*Be careful, layer values in code are going from 0 to 16, but they are corresponding to MIDI channels from 1 to 17.*

### Encoders

On each layer, you can use 16 different MIDI-CC capable rotary encoders : 8 by simply rotating encoders, 8 more by pressing the Shift button at the same time. There is 4 different modes of MIDI communication for these : an absolute mode (0-127 values are sent), and three relative modes (increment and decrement values are sent). To quicly reach some far away values, relative modes work with acceleration.

You can change the MIDI communication protocol called **MIDI_RELATIVE_MODE** for encoders by simply edit the [CONFIG.h](MIDI_CONTROLLER/CONFIG.h) file :

``` c++
#define MIDI_MODE RELATIVE_3 // MIDI mode for encoders
```

You can also customize the acceleration response of encoders :

``` c++
const uint8_t MAX_SPEED_ACC = 10;     // Maximum acceleration factor
const uint8_t MAX_TIME_ACC = 75;      // Maximum time threshold (ms) for acc.
const uint8_t ENCODER_MIN_STEP = 1;   // Encoder resolution
```

*Absolute and relative modes available here are standard for many of DAWs*

### Analog to MIDI-CC

This is an experimental feature that you can use by editing the [CONFIG.h](MIDI_CONTROLLER/CONFIG.h) file and uncomment the following line :

``` c++
#define ANALOG_ENABLED // Un-comment to enable the analog feature
```

You also can change all settings included in the [CONFIG.h](MIDI_CONTROLLER/CONFIG.h) file.

### Wiring diagram

```


                           ╔═══╗
                    ╔══════║   ║══════╗
      GND bus <---> ║G     ╚═══╝    5V║
       ENC_B7 <---> ║0            AGND║ <---> Analog GND bus
       ENC_A7 <---> ║1            3.3v║ <---> VCC bus
       ENC_B1 <---> ║2              23║ <---> ENC_A6
       ENC_A5 <---> ║3       T      22║ <---> ENC_B6
       ENC_B4 <---> ║4       E      21║ <---> ENC_A8
       ENC_A4 <---> ║5       E      20║ <---> ENC_B8
       ENC_B3 <---> ║6       N      19║ <---> [SCL] MPR_121
       ENC_A3 <---> ║7       S      18║ <---> [SDA] MPR_121
       ENC_B2 <---> ║8       Y      17║ <---> LED #2
       ENC_A2 <---> ║9              16║ <---> LED #1
       ENC_A1 <---> ║10             15║ <---> ADC #1
       ENC_B1 <---> ║11             14║ <---> ADC #2
        SHIFT <---> ║12             13║ <---> (Internal LED)
                    ╚═════════════════╝



```

### Bill of materials

- 1x :link: [Teensy LC](https://www.pjrc.com/store/teensylc.html)
- 2x :link: [Adafruit MPR_121](https://www.adafruit.com/product/1982)
- 8x :link: [Alps rotary encoders](https://fr.farnell.com/alps/ec12e2420803/encoder-vertical-12mm-24det-24ppr/dp/2065069)
- 2x LEDs with their appropriated resistor
- 1x :link: [Push button](https://fr.farnell.com/wurth-elektronik/430466043726/switch-260gf-12x12mm-4-3mm-act/dp/2065141)
- Some jumper cables
- 2x Large breadboards
- A capacitive keyboard (you can make your own with any conductive materials)

And that's it !

### Keyboard layout

#### Container [ 1-8 ] shift-layout

``` PlainText
╔═╦═══╦═══╦═══╦═╦═══╦═══╦═══╦═══╦═╦═══╦═╦═══╦═══╦═══╦═══╦═╦═══╦═══╗
║ ║ S ║   ║ T ║ ║ T ║   ║   ║ T ║ ║ T ║ ║ T ║   ║   ║ T ║ ║ T ║   ║
║ ║ A ║   ║ G ║ ║ G ║   ║   ║ G ║ ║ G ║ ║ G ║   ║   ║ G ║ ║ G ║   ║
║ ║ V ║   ║ L ║ ║ L ║   ║   ║ L ║ ║ L ║ ║ L ║   ║   ║ L ║ ║ L ║   ║
║ ║ E ║   ║ 1 ║ ║ 2 ║   ║   ║ 3 ║ ║ 4 ║ ║ 5 ║   ║   ║ 6 ║ ║ 7 ║   ║
║ ╚═══╣   ╚═╦═╝ ╚═╦═╝   ║   ╚═╦═╝ ╚═╦═╝ ╚═╦═╝   ║   ╚═╦═╝ ╚═╦═╝   ║
║  L  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║     ║
║  O  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║  R  ║
║  A  ║ [1] ║ [2] ║ [3] ║ [4] ║ [5] ║ [6] ║ [7] ║ [8] ║     ║  S  ║
║  D  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║  T  ║
╚═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╝
```

#### General [ 17 ] shift-layout

``` PlainText
╔═╦═══╦═══╦═══╦═╦═══╦═══╦═══╦═══╦═╦═══╦═╦═══╦═══╦═══╦═══╦═╦═══╦═══╗
║ ║ B ║   ║   ║ ║   ║   ║   ║ S ║ ║ L ║ ║ E ║   ║   ║ S ║ ║ S ║   ║
║ ║ P ║   ║ C ║ ║   ║   ║   ║ A ║ ║ O ║ ║ N ║   ║   ║ I ║ ║ I ║   ║
║ ║ M ║   ║ L ║ ║   ║   ║   ║ V ║ ║ A ║ ║ T ║   ║   ║ G ║ ║ G ║   ║
║ ║ + ║   ║ K ║ ║   ║   ║   ║ E ║ ║ D ║ ║ R ║   ║   ║ - ║ ║ + ║   ║
║ ╚═══╣   ╚═╦═╝ ╚═╦═╝   ║   ╚═╦═╝ ╚═╦═╝ ╚═╦═╝   ║   ╚═╦═╝ ╚═╦═╝   ║
║  B  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║     ║
║  P  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║  R  ║
║  M  ║ [1] ║ [2] ║ [3] ║ [4] ║ [5] ║ [6] ║ [7] ║ [8] ║     ║  S  ║
║  -  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║  T  ║
╚═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╝
```

or

``` PlainText
╔═╦═══╦═══╦═══╦═╦═══╦═══╦═══╦═══╦═╦═══╦═╦═══╦═══╦═══╦═══╦═╦═══╦═══╗
║ ║ S ║   ║ E ║ ║   ║   ║   ║ B ║ ║   ║ ║ B ║   ║   ║ S ║ ║ S ║   ║
║ ║ A ║   ║ N ║ ║   ║   ║   ║ P ║ ║ C ║ ║ P ║   ║   ║ I ║ ║ I ║   ║
║ ║ V ║   ║ T ║ ║   ║   ║   ║ M ║ ║ L ║ ║ M ║   ║   ║ G ║ ║ G ║   ║
║ ║ E ║   ║ R ║ ║   ║   ║   ║ - ║ ║ K ║ ║ + ║   ║   ║ - ║ ║ + ║   ║
║ ╚═══╣   ╚═╦═╝ ╚═╦═╝   ║   ╚═╦═╝ ╚═╦═╝ ╚═╦═╝   ║   ╚═╦═╝ ╚═╦═╝   ║
║  L  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║     ║
║  O  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║  R  ║
║  A  ║ [1] ║ [2] ║ [3] ║ [4] ║ [5] ║ [6] ║ [7] ║ [8] ║     ║  S  ║
║  D  ║     ║     ║     ║     ║     ║     ║     ║     ║     ║  T  ║
╚═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╝
```

#### Index :
- [N]  : Container n° N
- OCT  : Octave
- TGL  : Toggle
- CLK  : Clock
- RST  : Reset / MIDI panic
- BPM  : Clock beats per minute
- ENTR : Selection
- SIG  : Signature rythmique -/+

## Informations

### Build instructions

Run the following command line to clone this repository from your terminal :

``` bash
git clone --recursive https://gitlab.com/ilophone/ilophone-controller
```

*This allows you to get submodules. You can use a git client otherwise.*

Then, download the latest version of :link: [Teensyduino](https://www.pjrc.com/teensy/td_download.html), the latest compatible :link: [Arduino IDE](http://www.arduino.org/downloads) version, and open the MIDI_CONTROLLER.ino project file in the MIDI_CONTROLLER directory.

In the sketch tab, use the library manager to download the following libraries :
- ResponsiveAnalogRead
- Encoder
- Adafruit_MPR121

In the tools tab, choose **Teensy LC / 48MHz** as the board type, and **MIDIx4** as the USB type.

Now you can build the source code and upload the firmware to your board. Enjoy !

## Hacking

You can implement new features, or do anything else you know how to.
Don’t forget to share the result of your hack :pray:
