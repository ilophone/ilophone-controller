PRIORITY
---
- [x] Double click on shift ==> layer = 16
- [x] 24fps MIDI Clock

CLEAN :
---
- [ ] Clean enum declaration in touch.h
- [ ] Clean for loop encoder.cpp:47
- [x] Modular structure
- [x] Re-write all comments
- [x] Add license files

EXPERIMENTAL :
---
- [ ] Test if velocity is always working
- [ ] Check USB_MIDI dropout when reset MPR_121 on RPI
